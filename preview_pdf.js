var browser_is_firefox = function() {
    var isFirefox = typeof InstallTrigger !== 'undefined';
    return isFirefox;// Firefox 1.0+
};
function browser()
{
    var ua = navigator.userAgent;
    if (ua.search(/MSIE/) > 0) return 'Internet Explorer';
    if (ua.search(/Firefox/) > 0) return 'Firefox';
    if (ua.search(/Opera/) > 0) return 'Opera';
    if (ua.search(/Chrome/) > 0) return 'Google Chrome';
    if (ua.search(/Safari/) > 0) return 'Safari';
    if (ua.search(/Konqueror/) > 0) return 'Konqueror';
    if (ua.search(/Iceweasel/) > 0) return 'Debian Iceweasel';
    if (ua.search(/SeaMonkey/) > 0) return 'SeaMonkey';
    // Браузеров очень много, все вписывать смысле нет, Gecko почти везде встречается
    if (ua.search(/Gecko/) > 0) return 'Gecko';
    // а может это вообще поисковый робот
    return 'Search Bot';
}
var mob=false;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    mob=true;
} else {
    mob=false;
}
$(document).on('click', '.preview-panel .close', function() {
    iframe=window.$_this;
    iframe.off('scroll');
    iframe.off('wheel');
    iframe.off('keydown');
    iframe.off('resize');
    iframe.off('orientationchange');
    id_iframe = undefined;
});
function browser_safari(iframeDocument) {
    if(browser()==='Safari'){
        iframeDocument.image_clientHeight=iframeDocument.lazyloadImages[0].offsetHeight+10;
        window.myscroll=true;
    }
    else{
        iframeDocument.image_clientHeight=iframeDocument.lazyloadImages[0].offsetHeight;
    }

}
function inicialization(iframeDocument) {
    iframeDocument.lazyloadImages[0].style.willChange='transform';
    if(mob && screen.orientation.angle!=90){
        iframeDocument.lazyloadImages[0].style.height = "100%";
    }
    if(mob && screen.orientation.angle==90){
        iframeDocument.lazyloadImages[0].style.height = "auto";
    }
    iframeDocument.lazyloadImages[0].style.marginTop = "10";
    window.lazyloadThrottleTimeout;
    window.lazyloadThrottleTimeouts;
    window.lazyloadThrottleTimeouts_resize_time;
    iframeDocument.oneload_piture=true;
    window.scroll_vertical=false;
    window.myscroll=true;
}
function initlisten(iframe) {
    iframe.contentWindow.document.addEventListener("scroll", lazyload);
    iframe.contentWindow.document.addEventListener("wheel", lazyload);
    iframe.contentWindow.document.addEventListener("keydown", lazyload);
    if(mob){
        iframe.contentWindow.addEventListener("resize", lazyload_resize_mob);
    }
    else{
        iframe.contentWindow.addEventListener("resize", lazyload_resize);
    }
    if(browser_is_firefox()===true){
        window.addEventListener("orientationchange", lazyload_orintation_change);
    }
    else{
        iframe.contentWindow.addEventListener("orientationchange", lazyload_orintation_change);
    }

}
function img_listner_add(imgs,img,appends,id_frame,k,iframeDocument){
    imgs.addEventListener('load', function (e) {
        img.attr('src', location.origin+"/mflash/Dispatcher.php?C=PREVIEW&fid=" + id_frame + "&id_picture="+k);
        img.appendTo(appends);
        iframeDocument.lazyloadImages = iframe.contentWindow.document.querySelectorAll("img");
        iframeDocument.image_clientHeight=iframeDocument.lazyloadImages[0].offsetHeight;
        iframeDocument.lazyloadImages = iframe.contentWindow.document.querySelectorAll("img");
    });
}
function img_add () {
    if(mob  && screen.orientation.angle!=90){
         var img = $('<img src="" style="margin-top:10px;width:100%;height:100%;will-change: transform;" loading="lazy">'); //Equivalent: $(document.createElement('img'))
    }
    else{
        var img = $('<img src="" style="margin-top:10px;width:100%;will-change: transform;" loading="lazy">'); //Equivalent: $(document.createElement('img'))
    }
    return img;
}
function angle90_recount(iframeDocument) {
    iframeDocument.lazyloadImages = iframe.contentWindow.document.querySelectorAll("img");
    iframeDocument.lazyloadImages[0].style.height = "auto";
    iframeDocument.image_clientHeight=iframeDocument.lazyloadImages[0].offsetHeight;
    for (var g=1;g<iframeDocument.lazyloadImages.length;g++){
        iframeDocument.lazyloadImages[g].style.height = "auto";
        iframeDocument.image_clientHeight+=iframeDocument.lazyloadImages[g].offsetHeight;
    }

}
function angle_0_recount(iframeDocument) {
    iframeDocument.lazyloadImages = iframe.contentWindow.document.querySelectorAll("img");
    iframeDocument.lazyloadImages[0].style.height = "100%";
    iframeDocument.image_clientHeight=iframeDocument.lazyloadImages[0].offsetHeight;
    for (var f=1;f<iframeDocument.lazyloadImages.length;f++){
        iframeDocument.lazyloadImages[f].style.height = "100%";
        iframeDocument.image_clientHeight+=iframeDocument.lazyloadImages[f].offsetHeight;
    }
}
function images_error_log(imgs){
    imgs.addEventListener('error', function (e) {
        iframe.contentWindow.document.removeEventListener("scroll", lazyload);
        iframe.contentWindow.document.removeEventListener("wheel", lazyload);
        iframe.contentWindow.document.removeEventListener("keydown", lazyload);
        error = true;
    });
}
function angle_90_k1 (iframeDocument,k) {
    if(mob && screen.orientation.angle==90 && k==1 && iframeDocument.flags_orientation!==true){
        iframeDocument.lazyloadImages[0].style.height = "auto";
        iframeDocument.image_clientHeight=iframeDocument.lazyloadImages[0].offsetHeight;
    }
    if(mob && screen.orientation.angle!=90 && k==1 && iframeDocument.flags_orientation!==true){
        iframeDocument.lazyloadImages[0].style.height = "100%";
        iframeDocument.image_clientHeight=iframeDocument.lazyloadImages[0].offsetHeight;
    }
}
function scroll_vertical_bool(){
    if(window.scroll_vertical!==true){
        if(iframe.contentWindow.document.body.clientHeight === iframe.contentWindow.document.body.scrollHeight){
            window.scroll_vertical=false;
        }
        else{
            window.scroll_vertical=true;
        }
    }
}
function lazy_load_add_image () {
    if (mob && screen.orientation.angle != 90) {
        var img = $('<img src="" style="margin-top:10px;width:100%;height:100%;will-change: transform;" loading="lazy">'); //Equivalent: $(document.createElement('img'))
    } else {
        var img = $('<img src="" style="margin-top:10px;width:100%;will-change: transform;" loading="lazy">'); //Equivalent: $(document.createElement('img'))
    }
    return img;
}
function not_mob(object_iframe,iframeDocument,id_iframe) {
    iframeDocument.lazyloadImages = iframe.contentWindow.document.querySelectorAll("img");
    iframeDocument.image_clientHeight=iframeDocument.lazyloadImages[0].offsetHeight;
    for (var f=1;f<iframeDocument.lazyloadImages.length;f++){
        iframeDocument.image_clientHeight+=iframeDocument.lazyloadImages[f].offsetHeight;
    }
    var scrollTop = iframe.contentWindow.pageYOffset;
    var sum = iframe.contentWindow.innerHeight + scrollTop;
    iframe.contentWindow.scrollTo(0,10);
    if($(window).height()>iframeDocument.image_clientHeight){
        iframeDocument.alert_recount_picture=true;
        if(iframeDocument.oneload_piture===true){
            iframeDocument.lazyloadImages = iframe.contentWindow.document.querySelectorAll("img");
            var appends= object_iframe.contents().find("body");
            var img = img_add ();
            k=2;
            var imgs = new Image();
            imgs.src = location.origin+"/mflash/Dispatcher.php?C=PREVIEW&fid=" + id_iframe + "&id_picture="+k;
            img_listner_add(imgs,img,appends,id_iframe,k,iframeDocument);
            iframeDocument.oneload_piture=false;
        }
    }

}
function lazyloads(e,iframeDocument,myscroll_mob,id_iframe) {
    if(iframeDocument.alert_recount_picture===true){
        iframeDocument.lazyloadImages = iframe.contentWindow.document.querySelectorAll("img");
        iframeDocument.image_clientHeight=0;
        for (var f=0;f<iframeDocument.lazyloadImages.length;f++){
            iframeDocument.image_clientHeight+=iframeDocument.lazyloadImages[f].offsetHeight;
        }
        iframeDocument.alert_recount_picture=false;
    }
    var scrollTop = iframe.contentWindow.pageYOffset;
    angle_90_k1 (iframeDocument,k);
    if((window.myscroll===true || mob || e.type === 'wheel' || (e.keyCode > 36 && e.keyCode < 41)) && window.myscroll_mob===true) {
        for (var i = 0; i < iframeDocument.lazyloadImages.length; i++) {
            if (iframeDocument.image_clientHeight <= (iframe.contentWindow.innerHeight + scrollTop)) {
                k++;
                var src = location.origin+'/mflash/Dispatcher.php?C=PREVIEW&fid=' + id_iframe + '&id_picture=' + k;
                var appends = $("iframe.preview-iframe-pdf").contents().find("body");
                var img =lazy_load_add_image ();
                img.attr('src', location.origin+"/mflash/Dispatcher.php?C=PREVIEW&fid=" + id_iframe + "&id_picture=" + k);
                var imgs = new Image();
                imgs.src = location.origin+"/mflash/Dispatcher.php?C=PREVIEW&fid=" + id_iframe + "&id_picture=" + k;
                images_error_log(imgs);
                imgs.addEventListener('load', function (e) {
                    img.appendTo(appends);
                });
                iframeDocument.lazyloadImages = iframe.contentWindow.document.querySelectorAll("img");
                iframeDocument.image_clientHeight += iframeDocument.lazyloadImages[i].offsetHeight;
            }
        }
    }

}
if($('iframe.preview-iframe-pdf').css('display')=='none' && typeof id_iframe == 'undefined'){
    $('iframe.preview-iframe-pdf').on('load', function(){
        window.$_this=$(this);
        var iframe = document.getElementsByTagName('iframe')[0];
        window.iframe = document.getElementsByTagName('iframe')[0];
        var iframeDocument = iframe.contentDocument || iframe.contentWindow.document;
        iframeDocument.lazyloadImages = iframe.contentWindow.document.querySelectorAll("img");
        iframeDocument.lazyloadImages[0].style.width = "100%";
        if(browser()!=='Google Chrome'){
            iframeDocument.lazyloadImages[0].style.height = "auto";
        }
        $(this).show();
        inicialization(iframeDocument);
        window.myscroll_mob=true;
        browser_safari(iframeDocument);
        iframeDocument.imgs={};
        if(iframeDocument.alert_recount_picture!==true){
             k=1;
        }
        iframeDocument.alert_recount_picture=false;
        iframeDocument.flags_orientation=false;
        lazyload= function (e) {
            if(window.lazyloadThrottleTimeout) {
                clearTimeout(window.lazyloadThrottleTimeout);
            }
            window.lazyloadThrottleTimeout = setTimeout(function() {
                lazyloads(e,iframeDocument,myscroll_mob,fid);
            }, 20);
        }

        lazyload_orintation_change = function () {
            if(window.lazyloadThrottleTimeouts) {
                clearTimeout(window.lazyloadThrottleTimeouts);
            }
            window.lazyloadThrottleTimeouts = setTimeout(function() {
                $('.print_carousel').hide();
                if(k!==1){
                    iframeDocument.flags_orientation=true;
                }
                if(mob && screen.orientation.angle==90 && k>1){
                    angle90_recount(iframeDocument);
                }
                if(mob && screen.orientation.angle==0 && k>1){
                    angle_0_recount(iframeDocument);
                }
                if(mob && screen.orientation.angle!=90 && k==1){
                    iframeDocument.lazyloadImages[0].style.height = "100%";
                }
                if(mob && screen.orientation.angle==90 && k==1){
                    iframeDocument.lazyloadImages[0].style.height = "auto";
                }
            }, 30);

        }

        lazyload_resize_mob = function () {
            window.myscroll=false;
            setTimeout(function () {
                window.myscroll=true;
            },2000);
            scroll_vertical_bool();
            if( window.lazyloadThrottleTimeouts_resize_time) {
                clearTimeout( window.lazyloadThrottleTimeouts_resize_time);
            }
            window.lazyloadThrottleTimeouts_resize_time = setTimeout(function() {
                if(k!==1){
                    iframeDocument.flags_orientation=true;
                }
                if(mob){
                    iframeDocument.lazyloadImages = iframe.contentWindow.document.querySelectorAll("img");
                    iframeDocument.image_clientHeight=iframeDocument.lazyloadImages[0].offsetHeight;
                    for (var f=1;f<iframeDocument.lazyloadImages.length;f++){
                        iframeDocument.image_clientHeight+=iframeDocument.lazyloadImages[f].offsetHeight;
                    }
                    var scrollTop = iframe.contentWindow.pageYOffset;
                    var sum = iframe.contentWindow.innerHeight + scrollTop;
                    if($(window).height()>iframeDocument.image_clientHeight){
                        iframeDocument.alert_recount_picture=true;
                        if(iframeDocument.oneload_piture===true){
                            iframeDocument.lazyloadImages = iframe.contentWindow.document.querySelectorAll("img");
                            var appends= $("iframe.preview-iframe-pdf").contents().find("body");
                            var img = img_add ();
                            k=2;
                            var imgs = new Image();
                            imgs.src = location.origin+"/mflash/Dispatcher.php?C=PREVIEW&fid=" + fid + "&id_picture="+k;
                            img_listner_add(imgs,img,appends,fid,k,iframeDocument);
                            iframeDocument.oneload_piture=false;
                        }
                    }
                }
            }, 60);
        }

        lazyload_resize = function (e) {
            setTimeout(function () {
                if(!mob){
                    $('.print_pdf').show();
                }
            },1000);
            window.myscroll=false;
            setTimeout(function () {
                window.myscroll=true;
            },2000);
            //определяем видимость полосы прокрутки;
            scroll_vertical_bool();
            if(window.lazyloadThrottleTimeouts_resize_time) {
                clearTimeout( window.lazyloadThrottleTimeouts_resize_time);
            }
            window.lazyloadThrottleTimeouts_resize_time = setTimeout(function() {
                if(k!==1){
                    iframeDocument.flags_orientation=true;
                }
                if(!mob){
                    not_mob(window.$_this,iframeDocument,fid)
                }
            }, 60);

        }

        initlisten(iframe);

    });
}

if(typeof  id_iframe !== 'undefined'){
    $('#preview-iframe-pdf_' + id_iframe).on('load', function(){
        window.$_this=$(this);
        var iframe = document.getElementById('preview-iframe-pdf_' + id_iframe);
        window.iframe = document.getElementById('preview-iframe-pdf_' + id_iframe);
        var iframeDocument = iframe.contentDocument || iframe.contentWindow.document;
        iframeDocument.lazyloadImages = iframe.contentWindow.document.querySelectorAll("img");
        iframeDocument.lazyloadImages[0].style.width = "100%";
        if(browser()!=='Google Chrome'){
            iframeDocument.lazyloadImages[0].style.height = "auto";
        }
        $(this).show();
        if(!mob){
            $(this).hover(
                function() {
                    $('.print_carousel').show();
                },
                function() {
                    $('.print_carousel').show();
                }
            );
        }

        inicialization(iframeDocument)
        window.myscroll_mob=true;
        browser_safari(iframeDocument);
        iframeDocument.imgs={};
        if(iframeDocument.alert_recount_picture!==true){
            k=1;
        }
        iframeDocument.alert_recount_picture=false;
        iframeDocument.flags_orientation=false;
        lazyload = function  (e) {
            if($('#pdf_' + id_iframe).hasClass("active")) {
                if(window.lazyloadThrottleTimeout) {
                    clearTimeout(window.lazyloadThrottleTimeout);
                }
                window.lazyloadThrottleTimeout = setTimeout(function() {
                   // myscroll_mob=' && window.myscroll_mob===true';
                    lazyloads(e,iframeDocument,myscroll_mob,id_iframe)
                }, 20);
            }


        }

        lazyload_orintation_change= function () {
            if(window.lazyloadThrottleTimeouts) {
                clearTimeout(window.lazyloadThrottleTimeouts);
            }
            window.lazyloadThrottleTimeouts = setTimeout(function() {
                $('.print_carousel').hide();
                window.myscroll_mob=false;
                setTimeout(function () {
                    window.myscroll_mob=true;
                },2000);
                if(k!==1){
                    iframeDocument.flags_orientation=true;
                }
                if(mob && screen.orientation.angle==90 && k>1){
                    angle90_recount(iframeDocument);
                    $('.carousel-inner > .item').css('height', $(window).height()+'px');
                }
                if(mob && screen.orientation.angle==0 && k>1){
                    angle_0_recount(iframeDocument);
                    $('.carousel-inner > .item').css('height', $(window).height()+'px');
                }
                if(mob && screen.orientation.angle!=90 && k==1){
                    iframeDocument.lazyloadImages[0].style.height = "100%";
                    $('.carousel-inner > .item').css('height', $(window).height()+'px');
                }
                if(mob && screen.orientation.angle==90 && k==1){
                    iframeDocument.lazyloadImages[0].style.height = "auto";
                    $('.carousel-inner > .item').css('height', $(window).height()+'px');
                }
            }, 30);

        }

        lazyload_resize_mob = function () {
            scroll_vertical_bool();
            if(window.lazyloadThrottleTimeouts_resize_time) {
                clearTimeout(window.lazyloadThrottleTimeouts_resize_time);
            }
            window.lazyloadThrottleTimeouts_resize_time = setTimeout(function() {
                if(k!==1){
                    iframeDocument.flags_orientation=true;
                }
                if(mob){
                    iframeDocument.lazyloadImages = iframe.contentWindow.document.querySelectorAll("img");
                    iframeDocument.image_clientHeight=iframeDocument.lazyloadImages[0].offsetHeight;
                    for (var f=1;f<iframeDocument.lazyloadImages.length;f++){
                        iframeDocument.image_clientHeight+=iframeDocument.lazyloadImages[f].offsetHeight;
                    }
                    var scrollTop = iframe.contentWindow.pageYOffset;
                    var sum = iframe.contentWindow.innerHeight + scrollTop;
                    if(k!==1){
                        iframe.contentWindow.scrollTo(0,$(window).height()-100);
                    }
                    if($(window).height()>iframeDocument.image_clientHeight){
                        iframeDocument.alert_recount_picture=true;
                        if(iframeDocument.oneload_piture===true){
                            iframeDocument.lazyloadImages = iframe.contentWindow.document.querySelectorAll("img");
                            var appends= $("iframe.preview-iframe-pdf").contents().find("body");
                            var img = img_add ();
                            k=2;
                            var imgs = new Image();
                            imgs.src = location.origin+"/mflash/Dispatcher.php?C=PREVIEW&fid=" + id_iframe + "&id_picture="+k;
                            img_listner_add(imgs,img,appends,window.id_iframe,k,iframeDocument);
                            iframeDocument.oneload_piture=false;
                        }
                    }
                }
            }, 60);
        }

        lazyload_resize = function (e) {
            setTimeout(function () {
                if($('#pdf_' + id_iframe).hasClass("active")) {
                    //id_iframe='{{ preview.fid }}';
                    window.src_pdf='/mflash/Dispatcher.php?C=1000&file_id=' + id_iframe;
                    if(!mob){
                        $('.print_carousel').show();
                    }
                }
                else{
                    $('.print_carousel').hide();
                }
            },1000);

            window.myscroll=false;
            setTimeout(function () {
                window.myscroll=true;
            },2000);
            scroll_vertical_bool();
            if( window.lazyloadThrottleTimeouts_resize_time) {
                clearTimeout(window.lazyloadThrottleTimeouts_resize_time);
            }
            window.lazyloadThrottleTimeouts_resize_time = setTimeout(function() {
                if(k!==1){
                    iframeDocument.flags_orientation=true;
                }
                if(!mob){
                    not_mob(window.$_this,iframeDocument,id_iframe)
                }
            }, 60);

        }

        initlisten(iframe);

    });
}
